----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:07:53 06/25/2018 
-- Design Name: 
-- Module Name:    mux3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux3 is
    Port ( mempod : in  STD_LOGIC_VECTOR (31 downto 0);
           alu : in  STD_LOGIC_VECTOR (31 downto 0);
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0);
           ctrl : in  STD_LOGIC);
end mux3;

architecture Behavioral of mux3 is

begin
izlaz <= alu when ctrl='0' else mempod;

end Behavioral;

