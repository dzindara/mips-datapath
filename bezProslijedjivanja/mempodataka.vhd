----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:34:42 06/25/2018 
-- Design Name: 
-- Module Name:    mempodataka - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mempodataka is
 Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
		podatak: in std_logic_vector(31 downto 0);
		clk: in std_logic;
		we: in std_logic;
		izlaz : out std_logic_vector(31 downto 0)
		 );
end mempodataka;

architecture Behavioral of mempodataka is

type skup is array (0 to 2**10 -1) of std_logic_vector(31 downto 0);
signal set: skup :=(
others =>x"00000000"
);

begin

set(conv_integer(adresa))<= podatak when (clk'event and clk='1' and we='1');
izlaz<= set(conv_integer(adresa)) when (clk'event and clk='1' and we='0');
end Behavioral;

