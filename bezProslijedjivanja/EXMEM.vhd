----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:28:46 06/25/2018 
-- Design Name: 
-- Module Name:    EXMEM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXMEM is
    Port ( alu : in  STD_LOGIC_VECTOR (31 downto 0);
           aluo : out  STD_LOGIC_VECTOR (31 downto 0);
           r2 : in  STD_LOGIC_VECTOR (31 downto 0);
           r2o : out  STD_LOGIC_VECTOR (31 downto 0);
           memwb : in  STD_LOGIC_VECTOR (31 downto 0);
           memwbo : out  STD_LOGIC_VECTOR (31 downto 0);
			  clk : in std_logic);
end EXMEM;

architecture Behavioral of EXMEM is

begin
aluo <= alu when (clk'event and clk='1');
r2o <= r2 when (clk'event and clk='1');
memwbo <= memwb when (clk'event and clk='1');

end Behavioral;

