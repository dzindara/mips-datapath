----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:40:50 06/24/2018 
-- Design Name: 
-- Module Name:    IFID - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IFID is
    Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
           ir : in  STD_LOGIC_VECTOR (31 downto 0);
			  clk: in STD_LOGIC ;
			  ir1 : out std_logic_vector(4 downto 0);
			  ir2 : out std_logic_vector(4 downto 0);
			  se : out std_logic_vector(15 downto 0);
           adresao : out  STD_LOGIC_VECTOR (31 downto 0);
           iro : out  STD_LOGIC_VECTOR (31 downto 0));
			  
end IFID;

architecture Behavioral of IFID is

begin

adresao <= adresa when (clk'event and clk='1');
iro <= ir when (clk'event and clk='1');

ir1 <= ir(25 downto 21);
ir2 <= ir(20 downto 16);
se <= ir(15 downto 0);
end Behavioral;

