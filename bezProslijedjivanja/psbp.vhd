----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:14:36 06/25/2018 
-- Design Name: 
-- Module Name:    psbp - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity psbp is
    Port ( clk : in  STD_LOGIC);
end psbp;

architecture Behavioral of psbp is
component sabirac 
    Port ( ulaz : in  STD_LOGIC_VECTOR (31 downto 0);
           cetri : in  STD_LOGIC_VECTOR (2 downto 0):="100";
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component pc 
    Port ( ulaz : in  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC;
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end component;


component meminstrukcija
 Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
		instrukcija: out std_logic_vector(31 downto 0)
		 );
end component;

component IFID 
    Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
           ir : in  STD_LOGIC_VECTOR (31 downto 0);
			  clk: in STD_LOGIC ;
			  ir1 : out std_logic_vector(4 downto 0);
			  ir2 : out std_logic_vector(4 downto 0);
			  se : out std_logic_vector(15 downto 0);
           adresao : out  STD_LOGIC_VECTOR (31 downto 0);
           iro : out  STD_LOGIC_VECTOR (31 downto 0));
			  
end component;

component signextend 
    Port ( ulaz : in  STD_LOGIC_VECTOR (15 downto 0);
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component registri
    Port ( ir1 : in  STD_LOGIC_VECTOR (4 downto 0);
           ir2 : in  STD_LOGIC_VECTOR (4 downto 0);
			  clk: in std_logic;
           memwbinstr : in  STD_LOGIC_VECTOR (31 downto 0);
           podatak : in  STD_LOGIC_VECTOR (31 downto 0);
           registar1 : out  STD_LOGIC_VECTOR (31 downto 0);
           registar2 : out  STD_LOGIC_VECTOR (31 downto 0);
			  zaMux2 : out  STD_LOGIC
			  );
end component;

component IDEX 
    Port ( sabirac : in  STD_LOGIC_VECTOR (31 downto 0);
           reg1 : in  STD_LOGIC_VECTOR (31 downto 0);
           reg2 : in  STD_LOGIC_VECTOR (31 downto 0);
           se : in  STD_LOGIC_VECTOR (31 downto 0);
           ir : in  STD_LOGIC_VECTOR (31 downto 0);
           sabiraco : out  STD_LOGIC_VECTOR (31 downto 0);
           reg1o : out  STD_LOGIC_VECTOR (31 downto 0);
           reg2o : out  STD_LOGIC_VECTOR (31 downto 0);
           seo : out  STD_LOGIC_VECTOR (31 downto 0);
           iro : out  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC;
			  op:out std_logic_vector(2 downto 0);
			  we: out std_logic;
			  ctrlmux3: out std_logic
			  );
end component;

component mux1 
    Port ( idex : in  STD_LOGIC_VECTOR (31 downto 0);
           r1 : in  STD_LOGIC_VECTOR (31 downto 0);
           ctrl : in  STD_LOGIC;
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component mux2 
    Port ( r2 : in  STD_LOGIC_VECTOR (31 downto 0);
           se : in  STD_LOGIC_VECTOR (31 downto 0);
           ctrl : in  STD_LOGIC;
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end component;

component alu 
	port( a, b: in STD_LOGIC_VECTOR(31 downto 0);
	      ALUControl: in STD_LOGIC_VECTOR(2 downto 0); 
                                                   -- kako su samo cetiri operacije mo?e i dva bita
	      Result: buffer STD_LOGIC_VECTOR(31 downto 0);
	      ALUFlags: out STD_LOGIC_VECTOR(3 downto 0)); 
                                                   -- da umjesto cetiri izlaza bude jedan cetverobitni
end component;

component EXMEM 
    Port ( alu : in  STD_LOGIC_VECTOR (31 downto 0);
           aluo : out  STD_LOGIC_VECTOR (31 downto 0);
           r2 : in  STD_LOGIC_VECTOR (31 downto 0);
           r2o : out  STD_LOGIC_VECTOR (31 downto 0);
           memwb : in  STD_LOGIC_VECTOR (31 downto 0);
           memwbo : out  STD_LOGIC_VECTOR (31 downto 0);
			  clk : in std_logic);
end component;

component mempodataka 
 Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
		podatak: in std_logic_vector(31 downto 0);
		clk: in std_logic;
		we: in std_logic;
		izlaz : out std_logic_vector(31 downto 0)
		 );
end component;

component MEMWB 
    Port ( alu : in  STD_LOGIC_VECTOR (31 downto 0);
           mempod : in  STD_LOGIC_VECTOR (31 downto 0);
           aluo : out  STD_LOGIC_VECTOR (31 downto 0);
           mempodo : out  STD_LOGIC_VECTOR (31 downto 0);
			    memwb : in  STD_LOGIC_VECTOR (31 downto 0);
           memwbo : out  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC);
end component;

component mux3 
    Port ( mempod : in  STD_LOGIC_VECTOR (31 downto 0);
           alu : in  STD_LOGIC_VECTOR (31 downto 0);
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0);
           ctrl : in  STD_LOGIC
			  );
end component;


signal pcSabirac : std_logic_vector(31 downto 0);

signal izlazSabirac : std_logic_vector(31 downto 0);
signal izlazMeminstrukcija: std_logic_vector(31 downto 0);

signal IFIDadresa: std_logic_vector(31 downto 0);
signal IFIDir: std_logic_vector(31 downto 0);
signal IFIDir1: std_logic_vector(4 downto 0);
signal IFIDir2: std_logic_vector(4 downto 0);
signal IFIDse: std_logic_vector(15 downto 0);

signal izlazSignExtend : std_logic_vector(31 downto 0);

signal reg1: std_logic_vector(31 downto 0);
signal reg2: std_logic_vector(31 downto 0);
signal izRegzaCtrlMux2: std_logic;


signal IDEXsabiraco :   STD_LOGIC_VECTOR (31 downto 0);
signal IDEXreg1o :   STD_LOGIC_VECTOR (31 downto 0);
signal IDEXreg2o :   STD_LOGIC_VECTOR (31 downto 0);
signal IDEXseo :   STD_LOGIC_VECTOR (31 downto 0);
signal IDEXiro :   STD_LOGIC_VECTOR (31 downto 0);
signal IDEXop: std_logic_vector(2 downto 0);
signal IDEXwe: std_logic;
signal IDEXzam3: std_logic;
signal izlazMux1:STD_LOGIC_VECTOR (31 downto 0);
signal izlazMux2:STD_LOGIC_VECTOR (31 downto 0);

signal rezultatALU: std_logic_vector(31 downto 0);
signal zastavice: std_logic_vector(3 downto 0);

signal izlazAlu : STD_LOGIC_VECTOR (31 downto 0);
signal izlazr2 : STD_LOGIC_VECTOR (31 downto 0);
signal izlazmemwb : STD_LOGIC_VECTOR (31 downto 0);

signal izlazMemPodataka : std_logic_vector (31 downto 0);
signal ctrlzamux3 : std_logic;

signal izlazMemPod : std_logic_vector(31 downto 0);
signal izlazAluPregrada : std_logic_vector(31 downto 0);
signal memwbUZadnjiP : std_logic_vector(31 downto 0);

signal izlazZadnjegMuxa: std_logic_vector(31 downto 0);

begin
pcV: pc port map(izlazSabirac,clk,pcSabirac);
sabiracV: sabirac port map(pcSabirac,"100",izlazSabirac);
meminstrukcijaV: meminstrukcija port map(pcSabirac,izlazMeminstrukcija);
IFIDPregradni: IFID port map(izlazSabirac,izlazMeminstrukcija,clk,IFIDir1,IFIDir2,IFIDse,IFIDadresa,IFIDir);

signextendV: signextend port map(IFIDse,izlazSignExtend);
registriV: registri port map(IFIDir1,IFIDir2,clk,IFIDir,izlazZadnjegMuxa,reg1,reg2,izRegzaCtrlMux2);--izlazZadnjegMuxa
IDEXPregradni: IDEX port map(izlazSabirac,reg1,reg2,izlazSignExtend,IFIDir,IDEXsabiraco,IDEXreg1o,IDEXreg2o,IDEXseo,IDEXiro,clk,IDEXop,IDEXwe,IDEXzam3);

mux1V:mux1 port map(IDEXsabiraco,IDEXreg1o,'1',izlazMux1);
mux2V:mux2 port map(IDEXreg2o,IDEXseo,izRegzaCtrlMux2,izlazMux2);

aluV:alu port map(izlazMux1,izlazMux2,IDEXop,rezultatAlu,zastavice);
EXMEMPregradni: EXMEM port map(rezultatAlu,izlazAlu,IDEXreg2o,izlazr2,IDEXiro,izlazmemwb,clk);

mempodatakaV: mempodataka port map(izlazAlu,izlazr2,clk,IDEXwe,izlazMemPodataka);

MEMWBPregradni: MEMWB port map(izlazMemPodataka,izlazMemPod,izlazAlu,izlazAluPregrada,izlazmemwb,memwbUZadnjiP,clk);

mux3V: mux3 port map(izlazMemPod,izlazAluPregrada,izlazZadnjegMuxa,IDEXzam3);
end Behavioral;

