----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:18:33 06/25/2018 
-- Design Name: 
-- Module Name:    registri - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity registri is
    Port ( ir1 : in  STD_LOGIC_VECTOR (4 downto 0);
           ir2 : in  STD_LOGIC_VECTOR (4 downto 0);
			  clk: in std_logic;
           memwbinstr : in  STD_LOGIC_VECTOR (31 downto 0);
           podatak : in  STD_LOGIC_VECTOR (31 downto 0);
           registar1 : out  STD_LOGIC_VECTOR (31 downto 0);
           registar2 : out  STD_LOGIC_VECTOR (31 downto 0);
			   zaMux2 : out  STD_LOGIC
			  );
end registri;

architecture Behavioral of registri is

type registriskup is array (0 to 2**5 -1) of std_logic_vector(31 downto 0);
signal registri1: registriskup :=(
0=>x"00000000",
others =>x"00000000"
);


type skup is array (0 to 2**10 -1) of std_logic_vector(31 downto 0);
signal set: skup :=(
0=>x"11111111",--
1=>x"11111112",--
2=>x"11111113",--
3=>x"11111114",--
4=>x"11111115",--
5=>x"11111116",--
others =>x"00000000"
);


begin

registar1 <= registri1(conv_integer(ir1)) when (clk'event and clk='0');
registar2 <= registri1(conv_integer(ir2))when (clk'event and clk='0');
registri1(conv_integer(memwbinstr(15 downto 11)))<=podatak when (memwbinstr(31 downto 26)="000000" and  (clk'event and clk='1')); 
registri1(conv_integer(memwbinstr(20 downto 16)))	<=podatak when memwbinstr(31 downto 26) ="101011" and  (clk'event and clk='1');
zaMux2<= '0' when memwbinstr(31 downto 26)="000000" else '1'; -- zaMux2 koristimo ga kao kontrolni bit da znamo da li ?emo koristiti vrijednost drugog registra(tj. da li je trenutna instrukcija r tipa);
end Behavioral;

