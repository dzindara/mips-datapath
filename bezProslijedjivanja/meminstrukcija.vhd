----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:38:06 06/24/2018 
-- Design Name: 
-- Module Name:    meminstrukcija - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity meminstrukcija is
 Port ( adresa : in  STD_LOGIC_VECTOR (31 downto 0);
		instrukcija: out std_logic_vector(31 downto 0)
		 );
end meminstrukcija;

architecture Behavioral of meminstrukcija is

type skup is array (0 to 2**10 -1) of std_logic_vector(31 downto 0);
signal set: skup :=(
0=>x"11111111",--add
1=>x"11111112",--sub
2=>x"11111113",--and
3=>x"11111114",--or
4=>x"11111115",--lw
5=>x"11111116",--sw
others =>x"00000000"
);

begin 	

instrukcija <= set(conv_integer(adresa));
end Behavioral;

