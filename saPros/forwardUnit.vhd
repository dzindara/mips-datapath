----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:53:34 06/25/2018 
-- Design Name: 
-- Module Name:    forwardUnit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity forwardUnit is

  port (ID_EX_IR       : in  std_logic_vector(31 downto 0);
        EX_MEM_IR      : in  std_logic_vector(31 downto 0);
		EX_MEM_Rd      : in  std_logic_vector(4 downto 0);
        MEM_WB_Rd      : in  std_logic_vector(4 downto 0);
        --MEM_WB_Jmp_Src : in  std_logic;
        EX_MEM_Control : inout  std_logic_vector(11 downto 0);
        MEM_WB_Control : inout  std_logic_vector(11 downto 0);
        Fwd_A_Code     : out std_logic_vector(1 downto 0);
        Fwd_B_Code     : out std_logic_vector(1 downto 0)
		  
		  );
		  

end forwardUnit;

architecture Behavioral of forwardUnit is

  -- EX Stage Signals
  signal EX_MEM_Reg_Write_En   : std_logic;
  signal EX_MEM_Func           : std_logic_vector(5 downto 0);
  signal EX_MEM_Reg_Write      : std_logic;
  signal EX_MEM_Jmp_Src        : std_logic;
  signal EX_MEM_ALU_Op         : std_logic_vector(1 downto 0); 
  signal EX_MEM_ALU_Full_Code  : std_logic_vector(7 downto 0);
  signal EX_MEM_Rd_Non_Zero    : std_logic;
  signal ID_EX_Rs_Is_EX_MEM_Rd : std_logic;
  signal ID_EX_Rt_Is_EX_MEM_Rd : std_logic;
  signal ALU_A_EX_Compare      : std_logic_vector(4 downto 0);
  signal ALU_B_EX_Compare      : std_logic_vector(4 downto 0);

  -- MEM Stage Signals
  signal MEM_WB_Reg_Write_En   : std_logic;
  signal MEM_WB_Reg_Write      : std_logic;
  signal MEM_WB_Rd_Non_Zero    : std_logic;
  signal ID_EX_Rs_Is_MEM_WB_Rd : std_logic;
  signal ID_EX_Rt_Is_MEM_WB_Rd : std_logic;
  signal ALU_A_MEM_Compare     : std_logic_vector(4 downto 0);
  signal ALU_B_MEM_Compare     : std_logic_vector(4 downto 0);

  -- Others
  signal EX_A_Hazard           : std_logic;
  signal EX_B_Hazard           : std_logic;
  signal ID_EX_Rt              : std_logic_vector(4 downto 0);
  signal ID_EX_Rs              : std_logic_vector(4 downto 0);  

begin
EX_MEM_Control<=ID_EX_IR(31 downto 26) &ID_EX_IR(5 downto 0);


  -- Check if instruction in EX stage will write to a register
  EX_MEM_Reg_Write_En <= EX_MEM_Reg_Write AND (NOT EX_MEM_Jmp_Src);

  -- Determine if instruction in EX stage is a jr instruction
  EX_MEM_Func <= EX_MEM_IR( 5 downto  0);

  EX_MEM_Reg_Write <= EX_MEM_Control(8);
  EX_MEM_ALU_Op    <= EX_MEM_Control( 1 downto 0);

  EX_MEM_ALU_Full_Code <= EX_MEM_ALU_Op & EX_MEM_Func;

  with EX_MEM_ALU_Full_Code select
    EX_MEM_Jmp_Src <= '1' when "10000100", -- JR
                      '0' when others;   -- No JR


  -- Check if instruction in MEM stage will write to a register
  MEM_WB_Reg_Write <= MEM_WB_Control(8);
--  MEM_WB_Reg_Write_En <= MEM_WB_Reg_Write AND (NOT MEM_WB_Jmp_Src);

  -- Check if destination register in EX stage is non-zero
  EX_MEM_Rd_Non_Zero <= '0' when (EX_MEM_Rd = (EX_MEM_Rd'range => '0')) else '1';


  -- Check if destination register in MEM stage is non-zero
  MEM_WB_Rd_Non_Zero <= '0' when (MEM_WB_Rd = (MEM_WB_Rd'range => '0')) else '1';


  -- Check if ALU A source register in ID stage matches destination register in EX stage
  ID_EX_Rs <= ID_EX_IR(25 downto 21);

  ALU_A_EX_Compare <= ID_EX_Rs XOR EX_MEM_Rd;
  ID_EX_Rs_Is_EX_MEM_Rd <= '1' when (ALU_A_EX_Compare = (ALU_A_EX_Compare'range => '0')) else '0';

  -- Check if ALU A source register in ID stage matches destination register in MEM stage
  ALU_A_MEM_Compare <= ID_EX_Rs XOR MEM_WB_Rd;
  ID_EX_Rs_Is_MEM_WB_Rd <= '1' when (ALU_A_MEM_Compare = (ALU_A_MEM_Compare'range => '0')) else '0';


  -- Check if ALU B source register in ID stage matches destination register in EX stage
  ID_EX_Rt <= ID_EX_IR(20 downto 16);

  ALU_B_EX_Compare <= ID_EX_Rt XOR EX_MEM_Rd;
  ID_EX_Rt_Is_EX_MEM_Rd <= '1' when (ALU_B_EX_Compare = (ALU_B_EX_Compare'range => '0')) else '0';

  -- Check if ALU B source register in ID stage matches destination register in MEM stage
  ALU_B_MEM_Compare <= ID_EX_Rt XOR MEM_WB_Rd;
  ID_EX_Rt_Is_MEM_WB_Rd <= '1' when (ALU_B_MEM_Compare = (ALU_B_MEM_Compare'range => '0')) else '0';


  -- Determine if ALU A data hazard exists in EX stage or MEM stages
  EX_A_Hazard   <= EX_MEM_Reg_Write_En AND EX_MEM_Rd_Non_Zero AND ID_EX_Rs_Is_EX_MEM_Rd;
  Fwd_A_Code(1) <= EX_A_Hazard;
  Fwd_A_Code(0) <= MEM_WB_Reg_Write_En AND MEM_WB_Rd_Non_Zero AND ID_EX_Rs_Is_MEM_WB_Rd AND (NOT EX_A_Hazard);

  -- Determine if ALU B data hazard exists in EX stage or MEM stages
  EX_B_Hazard   <= EX_MEM_Reg_Write_En AND EX_MEM_Rd_Non_Zero AND ID_EX_Rt_Is_EX_MEM_Rd;
  Fwd_B_Code(1) <= EX_B_Hazard;
  Fwd_B_Code(0) <= MEM_WB_Reg_Write_En AND MEM_WB_Rd_Non_Zero AND ID_EX_Rt_Is_MEM_WB_Rd AND (NOT EX_B_Hazard);

end Behavioral;

