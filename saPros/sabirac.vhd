----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:24:53 06/24/2018 
-- Design Name: 
-- Module Name:    sabirac - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sabirac is
    Port ( ulaz : in  STD_LOGIC_VECTOR (31 downto 0);
           cetri : in  STD_LOGIC_VECTOR (2 downto 0):="100";
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end sabirac;

architecture Behavioral of sabirac is

begin

izlaz <= ulaz + cetri;
end Behavioral;

