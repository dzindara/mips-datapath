----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:15:33 06/25/2018 
-- Design Name: 
-- Module Name:    mux1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux1 is
    Port ( zadnjiMux : in  STD_LOGIC_VECTOR (31 downto 0);
           r1 : in  STD_LOGIC_VECTOR (31 downto 0);
           ctrl : in  STD_LOGIC_vector(1 downto 0);
			  izlazAlu: in std_logic_vector(31 downto 0);
           izlaz : out  STD_LOGIC_VECTOR (31 downto 0));
end mux1;

architecture Behavioral of mux1 is

begin

izlaz <= r1 when ctrl="00" else
	zadnjiMux when ctrl="01" else
	izlazAlu when ctrl="10"; --Zato ?to na?a ps ne podr?ava instrukcije grananja ovaj mux ?e samo proslije?ivati vrijednost r1(izlaz iz registara);


end Behavioral;

