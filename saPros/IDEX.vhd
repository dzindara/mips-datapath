----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:55:28 06/24/2018 
-- Design Name: 
-- Module Name:    IDEX - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IDEX is
    Port ( sabirac : in  STD_LOGIC_VECTOR (31 downto 0);
           reg1 : in  STD_LOGIC_VECTOR (31 downto 0);
           reg2 : in  STD_LOGIC_VECTOR (31 downto 0);
           se : in  STD_LOGIC_VECTOR (31 downto 0);
           ir : in  STD_LOGIC_VECTOR (31 downto 0);
           sabiraco : out  STD_LOGIC_VECTOR (31 downto 0);
           reg1o : out  STD_LOGIC_VECTOR (31 downto 0);
           reg2o : out  STD_LOGIC_VECTOR (31 downto 0);
           seo : out  STD_LOGIC_VECTOR (31 downto 0);
           iro : out  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC;
			    op:out std_logic_vector(2 downto 0);
				 we: out std_logic;
				 ctrlmux3: out std_logic
			  );
end IDEX;

architecture Behavioral of IDEX is

begin

sabiraco <= sabirac when (clk'event and clk='1');
reg1o <= reg1 when (clk'event and clk='1');
reg2o <= reg2 when (clk'event and clk='1');
seo <= se when (clk'event and clk='1');
iro <= ir when (clk'event and clk='1');

op<= "010" when sabirac(31 downto 26)="000000" and sabirac(5 downto 0)="100000" else
"110" when sabirac(31 downto 26)="000000" and sabirac(5 downto 0)="100010" else
"000" when sabirac(31 downto 26)="000000" and sabirac(5 downto 0)="100100" else
"001" when sabirac(31 downto 26)="000000" and sabirac(5 downto 0)="100101" else
"111";
--op koristimo da znamo koju operaciju ce izvrsiti alu ;

we<='1' when sabirac(31 downto 26)="101011" else '0'; -- write enable za memoriju podataka;
ctrlmux3<='1' when sabirac(31 downto 26)="100011" else '0'; -- ako je lw izlaz muxa3 je vr iz mem podataka u suprotnom iz alua
end Behavioral;

