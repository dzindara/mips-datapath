----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:48:30 06/25/2018 
-- Design Name: 
-- Module Name:    MEMWB - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MEMWB is
    Port ( alu : in  STD_LOGIC_VECTOR (31 downto 0);
           mempod : in  STD_LOGIC_VECTOR (31 downto 0);
           aluo : out  STD_LOGIC_VECTOR (31 downto 0);
           mempodo : out  STD_LOGIC_VECTOR (31 downto 0);
			    memwb : in  STD_LOGIC_VECTOR (31 downto 0);
           memwbo : out  STD_LOGIC_VECTOR (31 downto 0);
           clk : in  STD_LOGIC);
end MEMWB;

architecture Behavioral of MEMWB is

begin

aluo<=alu when (clk'event and clk='1');

mempodo<=mempod when (clk'event and clk='1');
end Behavioral;

